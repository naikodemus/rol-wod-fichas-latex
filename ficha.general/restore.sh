#! /bin/bash

declare -a criaturas=(vampiro humano garou ghoul changeling mago wraith)

[[ $# != 2 ]] \
	&& echo "[!!] Tipo de criatura y nombre del personaje…" \
	&& echo "[!!] Hay ${#criaturas[@]} opciones: ${criaturas[@]}" \
	&& exit 1

if [[ " ${criaturas[@]} " =~ " $1 " ]]; then
    cp -v $2 ./data/data.$1.csv
else
	echo "[!!] '$1' no es una criatura reconocida."
	echo "[!!] Hay ${#criaturas[@]} opciones: ${criaturas[@]}"
	exit 2
fi

echo ; echo DONE!
echo
exit 0
