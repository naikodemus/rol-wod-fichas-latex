#! /bin/bash

declare -a criaturas=(vampiro humano garou ghoul changeling mago wraith)

[[ $# != 2 ]] \
	&& echo "[!!] Tipo de criatura y nombre del personaje…" \
	&& echo "[!!] Hay ${#criaturas[@]} opciones: ${criaturas[@]}" \
	&& exit 1

if [[ " ${criaturas[@]} " =~ " $1 " ]]; then
    cp -v ./data/data.$1.csv ./xports/$2.csv
else
	echo "[!!] '$1' no es una criatura reconocida." \
	echo "[!!] Hay ${#criaturas[@]} opciones: ${criaturas[@]}"
	exit 2
fi

sed -i "s/§.*}/§$1}/" ficha.tex
lualatex -synctex=1 -interaction=nonstopmode ficha.tex

echo ; cp -v ficha.pdf ./xports/$2.pdf
echo ; ls -lh ./xports/
echo ; echo DONE!
echo
exit 0
