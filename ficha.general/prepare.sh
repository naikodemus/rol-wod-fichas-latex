#! /bin/bash

declare -a criaturas=(vampiro humano garou ghoul changeling mago wraith)

[[ $# != 1 ]] \
	&& echo "[!!] Hay ${#criaturas[@]} opciones: ${criaturas[@]}" \
	&& exit 1

if [[ " ${criaturas[@]} " =~ " $1 " ]]; then
    cp ./plantillas/$1/data.$1.csv ./data/data.$1.csv
else
	echo "[!!] '$1' no es una criatura reconocida."
	echo "[!!] Hay ${#criaturas[@]} opciones: ${criaturas[@]}"
	exit 2
fi

echo ; echo DONE!
echo
exit 0
