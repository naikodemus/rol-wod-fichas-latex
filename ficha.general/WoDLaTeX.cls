% escrito para lualatex
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{WoDLaTeX}[2020/02/11 World of Darkness LaTeX]
\LoadClass{article}

\DeclareOption{§vampiro}{\newcommand{\Bicho}{vampiro}}
\DeclareOption{§garou}{\newcommand{\Bicho}{garou}}
\DeclareOption{§humano}{\newcommand{\Bicho}{humano}}
\DeclareOption{§changeling}{\newcommand{\Bicho}{changeling}}
\DeclareOption{§ghoul}{\newcommand{\Bicho}{ghoul}}
\DeclareOption{§mago}{\newcommand{\Bicho}{mago}}
\DeclareOption{§wraith}{\newcommand{\Bicho}{wraith}}
\DeclareOption{Color}{\def\EnColor{1}}
\DeclareOption{Gris}{\def\EnColor{2}}
\DeclareOption{BlancoNegro}{\def\EnColor{3}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
%\ExecuteOptions{Vampiro,Color}
\ProcessOptions\relax

% MAQUETACIÓN
\usepackage{lipsum}
% \usepackage{showframe}

% VARIABLES
\newlength{\mpPrincip}
    \setlength{\mpPrincip}{25.75cm}

% GEOMETRÍA Y FORMATO
\usepackage[
    top=2.25cm,
    inner=1.0cm,
    outer=1.0cm,
    marginparsep=0.25cm,
    marginparwidth=0.5cm,
    ]{geometry}
% \headheight=0cm
\headsep=0cm
\textheight=28cm
% \footskip=0cm
% \parskip=0pt
\parindent=0pt

% TIPOGRAFÍA
\usepackage{microtype}
\usepackage{fontspec}
    \defaultfontfeatures{
        %  Path=Fonts/
        , Ligatures={TeX}
        , Ligatures={TeX,Common,Historic}
        % , Style=Historic
        , Scale=1.00, % original 1.0
        % , SizeFeatures={Size=\fontSize}
        % , UprightFont=*-Regular.otf
        % , BoldFont=*-Bold.otf
        % , ItalicFont=*-Italic.otf
        % , BoldItalicFont=*-BoldItalic.otf
        % , Kerning=On, % desactivar si está el paquete microtyp
        % , Style=TitlingCaps
        % , Mapping=tex-text % ligadura de los guiones
        }

% Tipografías principales
\setmainfont{TeX Gyre Pagella}  % Latin Modern Math, STIX, TeX Gyre Pagella, DejaVu Math TeX Gyre
%\setsansfont{DejaVu Sans}           % FreeSans
%\setmonofont{DejaVu Sans Mono}      % FreeMono, Unifont

% Tipografías auxiliares
\usepackage{moresize}
\usepackage{wasysym}
\newfontfamily{\amano}{ConcettaKalvani-Thin}

% PAGINACIÓN
\usepackage{fancyhdr}
    \fancypagestyle{ficha}{ %
        \fancyhf{} % remove everything
        \renewcommand{\headrulewidth}{0pt} % remove lines as well
        \renewcommand{\footrulewidth}{0pt}
        \fancyhf[HC]{%
            \Cartel
        }
    }
\pagestyle{ficha}

% LÓGICA Y MANIPULACIÓN DE DATOS
\usepackage{csvsimple}
    % carga automáticamente:
    % pgfkeys
    % etoolbox
    % ifthen
\usepackage{xstring}

% COLORES
\usepackage[usenames,dvipsnames]{xcolor}
\ifthenelse{\equal{\EnColor}{1}}{%
    \input{\Bicho/colores}
}{}
\ifthenelse{\equal{\EnColor}{2}}{%
    \definecolor{Gris}{gray}{0.5}
    \definecolor{GrisLigero}{gray}{0.75}
    \definecolor{GrisOscuro}{gray}{0.35}
    \definecolor{GrisCeniza}{gray}{0.75}
    \definecolor{RojoPuro}{gray}{0.5}
    \definecolor{RojoSangre}{gray}{0.25}
    \definecolor{VerdeBosque}{gray}{0.65}
}{}
\ifthenelse{\equal{\EnColor}{3}}{%
    \definecolor{Gris}{gray}{0}
    \definecolor{GrisLigero}{gray}{0.75}
    \definecolor{GrisOscuro}{gray}{0}
    \definecolor{GrisCeniza}{gray}{0}
    \definecolor{RojoPuro}{gray}{0}
    \definecolor{RojoSangre}{gray}{0}
    \definecolor{VerdeBosque}{gray}{0}
}{}

% GRÁFICOS
\usepackage{graphicx}
    \DeclareGraphicsExtensions{.pdf,.png,.jpg,.eps}
    \graphicspath{{\Bicho/img/}}

\usepackage{background}
\backgroundsetup{
    scale=1,
    color=black,
    opacity=1,
    angle=0,
    contents={\includegraphics[width=\paperwidth,height=\paperheight]{fondo}}%
    }%

% COLUMNAS
\usepackage{multicol}
    \setlength{\columnsep}{.5cm}
    \setlength{\columnseprule}{0.0pt}
    \def\columnseprulecolor{\color{Sepia}}

% TABLAS
\usepackage{tabularx}
    \setlength{\tabcolsep}{5pt}
    \renewcommand{\arraystretch}{1.5}
\usepackage{colortbl}

% FUNCIONES
\newenvironment{CajaEncabezado}[1]{%
    \begin{center}
    \begin{minipage}{#1}
    }{%
    \end{minipage}
    \end{center}
    }%

\newenvironment{CajaPrincipal}[1]{%
    \begin{center}
    \begin{minipage}{#1}
    }{%
    \end{minipage}
    \end{center}
    }%

% Carga del CSV filtrado y presentación de columna
\newlength{\tambol}
    \settowidth{\tambol}{○○○○○}
\newcommand{\rasgum}[2]{%
    \IfEqCase*{#2}{%
    {X}{#1  \dotfill \makebox[\tambol][c]{\color{GrisOscuro}\small(fracaso)}}%
    {D}{#1  \dotfill \makebox[\tambol][c]{\color{GrisOscuro}\small(dif. +2)}}%
    {0}{#1  \dotfill ○○○○○}%
    {1}{#1  \dotfill ●○○○○}%
    {2}{#1  \dotfill ●●○○○}%
    {3}{#1  \dotfill ●●●○○}%
    {4}{#1  \dotfill ●●●●○}%
    {5}{#1  \dotfill ●●●●●}%
    {6}{#1  \dotfill ●{\color{Gris}|}●●●●●}%
    {7}{#1  \dotfill ●●{\color{Gris}|}●●●●●}%
    {8}{#1  \dotfill ●●●{\color{Gris}|}●●●●●}%
    {9}{#1  \dotfill ●●●●{\color{Gris}|}●●●●●}%
    {10}{#1 \dotfill {\color{GrisLigero}………}●●●●●}%
    }[#1 \dotfill {\color{RojoPuro}(error)}]%
}%

\newcommand{\mdts}[2]{%
    \IfEqCase*{#2}{%
    {}{\dotfill}%
    {0}{\dotfill}%
    }[#1\dotfill{\small(#2)}]%
}%

\newcommand{\dbtrs}[2]{%
    \ifthenelse{\equal{#2}{}}{}{\def\locA{(#2)}}%
    \IfEqCase*{#1}{%
    {}{\dotfill}%
    {0}{\dotfill}%
    {X}{\dotfill}%
    }[#1 \locA\dotfill]%
}%

\newcommand{\Encabezado}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\TIPO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \textbf{\MakeUppercase{\RASGO}:}\dotfill{\color{RojoSangre}\DETALLE}\\
        }%
}%

\newcommand{\tituloA}[1]{%
    \vspace*{-1.0cm}
    \begin{center}
        \colorbox{white}{%
            \IfEqCase*{#1}{%
            {ATRIBUTOS}{\textbf{\large#1}}%
            {HABILIDADES}{\textbf{\large#1}}%
            }[\textbf{#1}]%
        }%
    \end{center}
    \vspace*{-0.6cm}
}

\newcommand{\tituloB}[1]{%
    \parbox[b][1em]{\columnwidth}{\centering\colorbox{white}{\textbf{#1}}}%
    \vspace*{0.1cm}
}

\newcommand{\tituloC}[1]{%
    \vspace*{-1.0cm}
    \begin{center}
        \colorbox{white}{%
            \IfEqCase*{#1}{%
            {ATRIBUTOS}{\textbf{\large#1}}%
            {HABILIDADES}{\textbf{\large#1}}%
            }[\textbf{#1}]%
        }%
    \end{center}
    \vspace*{-0.25cm}
}

\newcommand{\encabcol}[1]{%
    {\centering{\color{RojoSangre}\small\textasciitilde{} {\uppercase#1} \textasciitilde{}}}%
    \vspace*{0.05cm}%
}

\newcommand{\SecUpMov}{%
    \vspace{-0.5cm}%
}

\newcommand{\RasgoColumna}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\TIPO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \makebox[\columnwidth]{\rasgum{\RASGO}{\DETALLE}}
        }%
}%

\newcommand{\MyDs}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\TIPO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \makebox[\columnwidth]{\mdts{\RASGO}{\DETALLE}} % \RASGO\dotfill{\small(\DETALLE)}
        }%
}%

\newcommand{\BlqCentA}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\TIPO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \makebox[\columnwidth]{\dbtrs{\RASGO}{\DETALLE}} % \RASGO\dotfill{\small(\DETALLE)}
        }%
}%

\newcommand{\tbl}[1]{%
    {\ssmall\color{Gris}#1}
}

\newcommand{\dtc}[1]{%
    \IfEqCase*{#1}{%
    {}{\color{Gris}—}%
    {-}{\color{Gris}—}%
    {--}{\color{Gris}—}%
    {X}{\color{Gris}—}%
    }[#1]%
}

\newcommand{\comat}[1]{%
    \IfEqCase*{#1}{%
    {}{}%
    {-}{}%
    {--}{}%
    {X}{}%
    }[(#1)]%
}

\newcommand{\Shots}{%
    {\color{Gris}\framebox[2.5cm]{\phantom{□}}}
}

\newcommand{\Ataques}{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\EST}{ON}
        ]{data/data.ataques.\Bicho.csv}{}{%
        \TYP\dotfill\comat{\COM} &  \dtc{\OCC} & \dtc{\PRC} & \dtc{\DIF} & \dtc{\DAM} & \dtc{\AGR} & \dtc{\DIS} & \dtc{\CAD} & \dtc{\CRG} & \Shots\\
        }%
}%

\newcommand{\AsptcEqp}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\RASGO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \parbox{\columnwidth}{~\raggedright\amano\ifthenelse{\equal{\DETALLE}{}}{}{- \DETALLE}}
        }%
}%

\newcommand{\ContadorS}[1]{%
    \IfEqCase*{#1}{%
    {0}{○ ○ ○ ○ ○ ○ ○ ○ ○ ○}%
    {1}{● ○ ○ ○ ○ ○ ○ ○ ○ ○}%
    {2}{● ● ○ ○ ○ ○ ○ ○ ○ ○}%
    {3}{● ● ● ○ ○ ○ ○ ○ ○ ○}%
    {4}{● ● ● ● ○ ○ ○ ○ ○ ○}%
    {5}{● ● ● ● ● ○ ○ ○ ○ ○}%
    {6}{● ● ● ● ● ● ○ ○ ○ ○}%
    {7}{● ● ● ● ● ● ● ○ ○ ○}%
    {8}{● ● ● ● ● ● ● ● ○ ○}%
    {9}{● ● ● ● ● ● ● ● ● ○}%
    {10}{● ● ● ● ● ● ● ● ● ●}%
    }[{\color{RojoPuro}( e r r o r )}]%
}

\newcommand{\EvalVal}[1]{%
    \IfEqCase*{#1}{%
    {0}{\hspace{.5cm}}%
    {1}{-2}%
    {2}{-1}%
    {3}{-1}%
    {4}{0}%
    {5}{0}%
    {6}{0}%
    {7}{0}%
    {8}{+1}%
    {9}{+1}%
    {10}{+2}%
    }[{{\color{RojoPuro}(error)}}]%
}

\newcommand{\ReservaS}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\RASGO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \makebox[4.5cm][s]{\hspace{1mm}{\large\ContadorS{\DETALLE}}\hspace{1mm}}
        }%
}%

\newcommand{\Porte}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\RASGO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \makebox[4.5cm][c]{\hspace{1cm}\color{GrisOscuro}\small porte\dotfill(\EvalVal{\DETALLE})\hspace{1cm}}}
        }%
}%

\newcommand{\ReservaD}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\RASGO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \makebox[4.5cm][s]{\hspace{1mm}{\large\ContadorS{\DETALLE}}\hspace{1mm}}
        }%
        \makebox[4.5cm][s]{\hspace{1.15mm}{\large□ □ □ □ □ □ □ □ □ □}\hspace{1mm}}
}%

\newcommand{\Pagina}[1]{
    \input{\Bicho/pagina#1}}
}

\newcommand{\EncabezadoSeg}[1]{%
    \csvreader[
          head to column names,
        , /csv/filter equal={\RASGO}{#1}
        ]{data/data.\Bicho.csv}{}{%
        \textbf{\MakeUppercase{\RASGO}:}\dotfill{\color{RojoSangre}\DETALLE}\\
        }%
}%

\ifthenelse{\equal{\Bicho}{vampiro}}{%
    \input{\Bicho/funciones}
}{}

\ifthenelse{\equal{\Bicho}{garou}}{%
    \input{\Bicho/funciones}
}{}

\ifthenelse{\equal{\Bicho}{humano}}{%
    \input{\Bicho/funciones}
}{}

\ifthenelse{\equal{\Bicho}{changeling}}{%
    \input{\Bicho/funciones}
}{}

\ifthenelse{\equal{\Bicho}{ghoul}}{%
    \input{\Bicho/funciones}
}{}

\ifthenelse{\equal{\Bicho}{mago}}{%
    \input{\Bicho/funciones}
}{}

\ifthenelse{\equal{\Bicho}{wraith}}{%
    \input{\Bicho/funciones}
}{}